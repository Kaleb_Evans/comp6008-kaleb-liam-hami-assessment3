﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace assessment2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailPage : ContentPage
    {
        public DetailPage(string URL, string DESC, string Title, string Number)
        {
            InitializeComponent();

            _image.Source = URL;
            Desc.Text = DESC;
            Name.Text = Title;
            Rating.Text = Number;

        }
    }
}

